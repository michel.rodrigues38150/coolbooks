import Vue from 'vue'
import VueRouter from 'vue-router'

import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'

import '@/assets/css/font-face.css';
import '@/assets/css/all.min.css';

import '@/assets/vendor/font-awesome-4.7/css/font-awesome.min.css';
import '@/assets/vendor/font-awesome-5/css/fontawesome-all.min.css';

import '@/assets/vendor/mdi-font/css/material-design-iconic-font.min.css';

import '@/assets/css/theme.css';


import App from './App.vue'


Vue.config.productionTip = false
Vue.use(VueRouter)

const routes = [
    {
      path: '/', name:'home',
        component: () => import('./components/bookComponent.vue')
    },
    {
        path: '/genres', name:'genres',
        component: () => import('./components/bookGenres.vue')
    },
    {
        path: '/books', name:'books',
        component: () => import('./components/bookComponent.vue')
    },
    {
        path: '/books/:bookId', name:'bookDetails',
        component: () => import('./components/bookDetails.vue')
    },
    {
      path: '/admin',name:'adminComponent',
      component: ()=> import('./components/adminComponent.vue')
    },
    {
      path: 'admin/genre/create',name:'addGenre',
      component: ()=> import('./components/addGenre.vue')
    }
]

const router = new VueRouter({
  routes
})

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
